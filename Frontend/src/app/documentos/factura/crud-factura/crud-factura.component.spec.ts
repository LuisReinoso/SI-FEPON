import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudFacturaComponent } from './crud-factura.component';
import { SharedModule } from '@app/shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgbModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { TagInputModule } from 'ngx-chips';
import { FacturaService, EtiquetaService } from '@app/shared/servicios';
import { HttpClient } from '@angular/common/http';
import {
  HttpService,
  ApiPrefixInterceptor,
  ErrorHandlerInterceptor,
  AuthenticationService,
  HttpCacheService,
  CacheInterceptor
} from '@app/core';
import { HeadersInterceptor } from '@app/core/http/headers.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { getIFacturaMock } from '@app/core/models';
import { getIModeloArchivoCompletoMock } from '@app/core/models/modelo-archivo.mock';

describe('CrudFacturaComponent', () => {
  let component: CrudFacturaComponent;
  let fixture: ComponentFixture<CrudFacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        SharedModule,
        HttpClientTestingModule,
        TagInputModule,
        NgbDatepickerModule,
        BrowserAnimationsModule,
        NgbModule
      ],
      providers: [
        ApiPrefixInterceptor,
        ErrorHandlerInterceptor,
        HeadersInterceptor,
        AuthenticationService,
        HttpCacheService,
        CacheInterceptor,
        FacturaService,
        EtiquetaService,
        {
          provide: HttpClient,
          useClass: HttpService
        },
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of({
              id: 'abcdefg'
            })
          }
        }
      ],
      declarations: [CrudFacturaComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia cargar factura desde input factura', () => {
    // GIVEN
    spyOn(component.factura, 'next');

    // WHEN
    component.cargarFactura = getIFacturaMock({ id: '123456789', fecha: new Date().getTime() });

    // THEN
    expect(component.factura.next).toHaveBeenCalled();
  });

  it('deberia descargar factura dado un id de queryParams', () => {
    // GIVEN
    spyOn(component, 'descargarFactura');

    // WHEN
    component.ngOnInit();

    // THEN
    expect(component.descargarFactura).toHaveBeenCalled();
  });

  it('deberia configurar el formulario de la factura desde input factura', () => {
    // GIVEN
    const auxFactura = <any>getIFacturaMock({ id: '123456789', fecha: new Date().getTime() });
    auxFactura['creado'] = new Date().getTime();
    auxFactura['actualizado'] = new Date().getTime();
    component.cargarFactura = auxFactura;

    // WHEN
    component.ngOnInit();

    // THEN
    expect(component.facturaForm.controls.id.value).toBe('123456789');
  });

  it('deberia agregar rubro a subtotaldetalle en detalle facturaForm', () => {
    // GIVEN

    // WHEN
    component.agregarRubroASubtotalDetalle();

    // THEN
    expect(component.facturaForm.controls.detalle.value.length).toBe(1);
  });

  it('deberia eliminar rubro a subtotaldetalle en detalle facturaForm', () => {
    // GIVEN
    component.agregarRubroASubtotalDetalle();

    // WHEN
    component.eliminarRubroASubtotalDetalle(0);

    // THEN
    expect(component.facturaForm.controls.detalle.value.length).toBe(0);
  });

  it('deberia agregar rubro a subtotaldetalletotal en detalletotal facturaForm', () => {
    // GIVEN

    // WHEN
    component.agregarRubroASubtotalDetalleTotal();

    // THEN
    expect(component.facturaForm.controls.detalleTotal.value.length).toBe(1);
  });

  it('deberia eliminar rubro a subtotaldetalletotal en detalletotal facturaForm', () => {
    // GIVEN
    component.agregarRubroASubtotalDetalleTotal();

    // WHEN
    component.eliminarRubroASubtotalDetalleTotal(0);

    // THEN
    expect(component.facturaForm.controls.detalleTotal.value.length).toBe(0);
  });

  it('deberia sincronizar los archivos almacenados con crud factura', () => {
    // GIVEN
    const archivos = [getIModeloArchivoCompletoMock()];

    // WHEN
    component.sincronizacionArchivosAlmacenados(archivos);

    // THEN
    expect(component.archivosAlmacenados.length).toBe(1);
  });

  it('deberia guardar factura dado un facturaForm valido', () => {
    // GIVEN
    const factura = <any>getIFacturaMock({
      id: '',
      nombreEmisor: 'Juan Perez',
      rucEmisor: '123456789',
      numeroFactura: '00001',
      nombreReceptor: 'Paulina Rosero',
      rucReceptor: '987456321',
      fecha: {
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        day: new Date().getDate()
      }
    });
    factura['creado'] = new Date().getTime();
    factura['actualizado'] = new Date().getTime();
    component.facturaForm.setValue(factura);

    const facturaService = fixture.debugElement.injector.get(FacturaService);
    spyOn(facturaService, 'create').and.returnValue(of(getIFacturaMock({ id: '123456789' })));

    // WHEN
    component.guardarFactura();

    // THEN
    expect(component.facturaForm.controls.id.value).toBe('123456789');
  });

  it('deberia marcar como tocado el facturaForm en guardarFactura dado un facturaForm invalido', () => {
    // GIVEN
    const factura = <any>getIFacturaMock();
    factura['creado'] = new Date().getTime();
    factura['actualizado'] = new Date().getTime();
    component.facturaForm.setValue(factura);

    // WHEN
    component.guardarFactura();

    // THEN
    expect(component.facturaForm.controls.nombreEmisor.touched).toBeTruthy();
    expect(component.facturaForm.controls.rucEmisor.touched).toBeTruthy();
    expect(component.facturaForm.controls.numeroFactura.touched).toBeTruthy();
    expect(component.facturaForm.controls.nombreReceptor.touched).toBeTruthy();
    expect(component.facturaForm.controls.rucReceptor.touched).toBeTruthy();
    expect(component.facturaForm.controls.fecha.touched).toBeTruthy();
  });

  it('deberia actualizar factura dado un facturaForm valido', () => {
    // GIVEN
    const factura = <any>getIFacturaMock({
      id: '',
      nombreEmisor: 'Juan Perez',
      rucEmisor: '123456789',
      numeroFactura: '00001',
      nombreReceptor: 'Paulina Rosero',
      rucReceptor: '987456321',
      fecha: {
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        day: new Date().getDate()
      }
    });
    factura['creado'] = new Date().getTime();
    factura['actualizado'] = new Date().getTime();
    component.facturaForm.setValue(factura);

    const facturaService = fixture.debugElement.injector.get(FacturaService);
    spyOn(facturaService, 'update').and.returnValue(of(getIFacturaMock({ id: '123456789' })));

    // WHEN
    component.actualizarFactura();

    // THEN
    expect(facturaService.update).toHaveBeenCalled();
  });

  it('deberia marcar como tocado el facturaForm en actualizarFactura dado un facturaForm invalido', () => {
    // GIVEN
    const factura = <any>getIFacturaMock();
    factura['creado'] = new Date().getTime();
    factura['actualizado'] = new Date().getTime();
    component.facturaForm.setValue(factura);

    // WHEN
    component.actualizarFactura();

    // THEN
    expect(component.facturaForm.controls.nombreEmisor.touched).toBeTruthy();
    expect(component.facturaForm.controls.rucEmisor.touched).toBeTruthy();
    expect(component.facturaForm.controls.numeroFactura.touched).toBeTruthy();
    expect(component.facturaForm.controls.nombreReceptor.touched).toBeTruthy();
    expect(component.facturaForm.controls.rucReceptor.touched).toBeTruthy();
    expect(component.facturaForm.controls.fecha.touched).toBeTruthy();
  });

  it('deberia eliminar factura dado un facturaForm valido', () => {
    // GIVEN
    const factura = <any>getIFacturaMock({
      id: '123456789',
      nombreEmisor: 'Juan Perez',
      rucEmisor: '123456789',
      numeroFactura: '00001',
      nombreReceptor: 'Paulina Rosero',
      rucReceptor: '987456321',
      fecha: {
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        day: new Date().getDate()
      }
    });
    factura['creado'] = new Date().getTime();
    factura['actualizado'] = new Date().getTime();
    component.facturaForm.setValue(factura);

    const facturaService = fixture.debugElement.injector.get(FacturaService);
    spyOn(facturaService, 'destroy').and.returnValue(of(getIFacturaMock({ id: '123456789' })));

    // WHEN
    component.eliminarFactura();

    // THEN
    expect(facturaService.destroy).toHaveBeenCalled();
    expect(component.facturaForm.untouched).toBeTruthy();
  });

  it('deberia realizar los calculos total de la factura', () => {
    // GIVEN
    const factura = <any>getIFacturaMock({
      id: '123456789',
      nombreEmisor: 'Juan Perez',
      rucEmisor: '123456789',
      numeroFactura: '00001',
      nombreReceptor: 'Paulina Rosero',
      rucReceptor: '987456321',
      fecha: {
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        day: new Date().getDate()
      },
      detalle: [{ cantidad: 5, descripcion: 'cola', precioUnitario: 5, total: 0 }]
    });
    factura['creado'] = new Date().getTime();
    factura['actualizado'] = new Date().getTime();
    component.facturaForm.setValue(factura);

    // WHEN
    component.calculoTotalFactura();

    // THEN
    expect(component.facturaForm.controls.subTotalDetalle.value).toBe(25);
  });

  it('deberia activar formControl iva dado isIva verdadero', () => {
    // GIVEN
    const factura = <any>getIFacturaMock({
      id: '123456789',
      nombreEmisor: 'Juan Perez',
      rucEmisor: '123456789',
      numeroFactura: '00001',
      nombreReceptor: 'Paulina Rosero',
      rucReceptor: '987456321',
      fecha: {
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        day: new Date().getDate()
      },
      isIva: true
    });
    factura['creado'] = new Date().getTime();
    factura['actualizado'] = new Date().getTime();
    component.facturaForm.setValue(factura);

    // WHEN
    component.cambiarActivacionIva();

    // THEN
    expect(component.facturaForm.controls.iva.status).not.toBe('DISABLED');
  });

  it('deberia desactivar formControl iva dado isIva false', () => {
    // GIVEN
    const factura = <any>getIFacturaMock({
      id: '123456789',
      nombreEmisor: 'Juan Perez',
      rucEmisor: '123456789',
      numeroFactura: '00001',
      nombreReceptor: 'Paulina Rosero',
      rucReceptor: '987456321',
      fecha: {
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        day: new Date().getDate()
      },
      isIva: false
    });
    factura['creado'] = new Date().getTime();
    factura['actualizado'] = new Date().getTime();
    component.facturaForm.setValue(factura);

    // WHEN
    component.cambiarActivacionIva();

    // THEN
    expect(component.facturaForm.controls.iva.status).toBe('DISABLED');
  });

  it('deberia descargarEtiquetas dado el nombre', () => {
    // GIVEN
    const nombre = 'zanahorias';
    const etiquetaService = fixture.debugElement.injector.get(EtiquetaService);
    spyOn(etiquetaService, 'findWhere');

    // WHEN
    component.descargarEtiquetas(nombre);

    // THEN
    expect(etiquetaService.findWhere).toHaveBeenCalled();
  });

  it('deberia descargarFactura dado el idFactura', () => {
    // GIVEN
    const idFactura = '123456789';
    const factura = <any>getIFacturaMock({
      id: '123456789',
      nombreEmisor: 'Juan Perez',
      rucEmisor: '123456789',
      numeroFactura: '00001',
      nombreReceptor: 'Paulina Rosero',
      rucReceptor: '987456321',
      fecha: new Date().getTime(),
      isIva: false
    });
    factura['creado'] = new Date().getTime();
    factura['actualizado'] = new Date().getTime();
    const facturaService = fixture.debugElement.injector.get(FacturaService);
    spyOn(facturaService, 'findOne').and.returnValue(of(factura));

    // WHEN
    component.ngOnInit();
    component.descargarEtiquetas(idFactura);

    // THEN
    expect(component.archivosAlmacenados.length).toBe(0);
    expect(component.facturaForm.controls.id.value).toBe('123456789');
  });
});
