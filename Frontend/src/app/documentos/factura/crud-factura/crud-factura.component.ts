import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IModeloArchivoCompleto, markFormGroupTouched } from '@app/shared/utilidades.util';
import { Logger } from '@app/core';
import { IFactura, IEtiqueta, Factura } from '@app/core/models';
import { EtiquetaService, FacturaService } from '@app/shared/servicios';
import { Observable, Subject } from 'rxjs';
import Query from 'waterline-query-language-parser';
import { ActivatedRoute } from '@angular/router';

const log = new Logger('Crud factura');

@Component({
  selector: 'app-crud-factura',
  templateUrl: './crud-factura.component.html',
  styleUrls: ['./crud-factura.component.scss'],
  providers: [EtiquetaService, FacturaService]
})
export class CrudFacturaComponent implements OnInit {
  @Input('factura')
  set cargarFactura(factura: IFactura) {
    if (factura) {
      const auxFactura = { ...new Factura(factura).toJson(), ...factura };
      this.factura.next(auxFactura);
    }
  }

  facturaForm: FormGroup = this.fb.group({
    nombreEmisor: ['', Validators.required],
    rucEmisor: ['', Validators.required],
    numeroFactura: ['', Validators.required],
    nombreReceptor: ['', Validators.required],
    rucReceptor: ['', Validators.required],
    detalle: [[], null],
    subTotalDetalle: [0, null],
    detalleTotal: [[], null],
    subTotalDetalleTotal: [0, null],
    comentario: ['', null],
    total: [0, null],
    isIngreso: [false, null],
    imagenes: [[], null],
    isIva: [true, null],
    valorIva: [0, null],
    iva: [12, null],
    fecha: [
      {
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        day: new Date().getDate()
      },
      Validators.required
    ],
    etiquetas: [[], null],
    creado: [''],
    actualizado: [''],
    id: ['']
  });

  etiquetas: IEtiqueta[] = [];
  archivosAlmacenados: IModeloArchivoCompleto[] = [];

  factura = new Subject<IFactura>();

  constructor(
    private fb: FormBuilder,
    private facturaService: FacturaService,
    private etiquetaService: EtiquetaService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['id']) {
        this.descargarFactura(params['id']);
      }
    });

    this.factura.subscribe(factura => {
      factura.fecha = {
        year: new Date(<number>factura.fecha).getFullYear(),
        month: new Date(<number>factura.fecha).getMonth() + 1,
        day: new Date(<number>factura.fecha).getDate()
      };
      this.archivosAlmacenados = factura.imagenes;
      this.facturaForm.setValue(factura);
    });
  }

  agregarRubroASubtotalDetalle() {
    this.facturaForm.controls.detalle.value.push({ cantidad: 0, descripcion: '', precioUnitario: 0, total: 0 });
  }

  eliminarRubroASubtotalDetalle(indiceRubro: number) {
    this.facturaForm.controls.detalle.value.splice(indiceRubro, 1);
  }

  agregarRubroASubtotalDetalleTotal() {
    this.facturaForm.controls.detalleTotal.value.push({ cantidad: 0, nombre: '' });
  }

  eliminarRubroASubtotalDetalleTotal(indiceRubro: number) {
    this.facturaForm.controls.detalleTotal.value.splice(indiceRubro, 1);
  }

  /**
   * Obtiene todos los archivos almacenados que le pertenece a la factura
   *
   * @param {*} $event
   * @memberof CrudFacturaComponent
   */
  sincronizacionArchivosAlmacenados(event: IModeloArchivoCompleto[]) {
    this.archivosAlmacenados = event;
  }

  guardarFactura() {
    if (this.facturaForm.valid) {
      this.facturaForm.controls.imagenes.setValue(this.archivosAlmacenados);
      const modeloFactura = new Factura(this.facturaForm.value).toJson();
      delete modeloFactura['id'];
      this.facturaService.create(modeloFactura).subscribe(
        (factura: IFactura) => {
          this.facturaForm.controls.id.setValue(factura.id);
        },
        error => log.error(error)
      );
    } else {
      markFormGroupTouched(this.facturaForm);
    }
  }

  actualizarFactura() {
    if (this.facturaForm.valid) {
      this.facturaForm.controls.imagenes.setValue(this.archivosAlmacenados);
      const modeloFactura = new Factura(this.facturaForm.value).toJson();
      this.facturaService.update(modeloFactura).subscribe((factura: IFactura) => {}, error => log.error(error));
    } else {
      markFormGroupTouched(this.facturaForm);
    }
  }

  eliminarFactura() {
    this.facturaService.destroy(this.facturaForm.value).subscribe(
      factura => {
        this.facturaForm.reset();
      },
      error => log.error(error)
    );
  }

  calculoTotalFactura() {
    const factura = new Factura(this.facturaForm.value);
    factura.calculoTotal();
    this.facturaForm.controls.total.setValue(factura.total);
    this.facturaForm.controls.valorIva.setValue(factura.valorIva);
    this.facturaForm.controls.subTotalDetalle.setValue(factura.subTotalDetalle);
    this.facturaForm.controls.subTotalDetalleTotal.setValue(factura.subTotalDetalleTotal);
  }

  cambiarActivacionIva() {
    if (this.facturaForm.controls.isIva.value) {
      this.facturaForm.controls.iva.enable();
    } else {
      this.facturaForm.controls.iva.disable();
    }
  }

  public descargarEtiquetas = (nombre: string): Observable<Response> => {
    const query = new Query(`nombre: ${nombre} categoria: ${nombre}`);
    return <Observable<Response>>this.etiquetaService.findWhere(query.query);
  };

  descargarFactura(idFactura: string) {
    this.facturaService.findOne(idFactura).subscribe((factura: IFactura) => {
      factura.fecha = {
        year: new Date(<number>factura.fecha).getFullYear(),
        month: new Date(<number>factura.fecha).getMonth() + 1,
        day: new Date(<number>factura.fecha).getDate()
      };
      this.archivosAlmacenados = factura.imagenes;
      this.facturaForm.setValue(factura);
    });
  }
}
