import { Injectable } from '@angular/core';
import { BaseRest } from './base-rest';
import { HttpClient } from '@angular/common/http';
import { Logger } from '@app/core';

const log = new Logger('Etiqueta Service');

@Injectable()
export class EtiquetaService extends BaseRest {
  constructor(http: HttpClient) {
    super(http, 'Etiqueta');
  }
}
