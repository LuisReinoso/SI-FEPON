import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { DashboardEtiquetaComponent } from './dashboard-etiqueta/dashboard-etiqueta.component';
import { DetalleEtiquetaComponent } from './detalle-etiqueta/detalle-etiqueta.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: DashboardEtiquetaComponent,
        data: { title: extract('Etiqueta - dashboard') }
      },
      {
        path: 'detalle',
        component: DetalleEtiquetaComponent,
        data: { title: extract('Etiqueta - detalle') }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class EtiquetaRoutingModule {}
