import { IModeloArchivoCompleto } from '@app/shared/utilidades.util';

const getDefaults = (): IModeloArchivoCompleto =>
  <IModeloArchivoCompleto>{
    nombreArchivoOriginal: 'archivo.txt',
    nombreArchivo: 'asdasdsad.txt',
    ubicacion: '/home/luis/documentos/',
    url: 'documentos/asdasdsad.txt'
  };

const getIModeloArchivoCompletoMock = (p?: Partial<IModeloArchivoCompleto>): IModeloArchivoCompleto => {
  return <IModeloArchivoCompleto>{
    ...getDefaults(),
    ...p
  };
};

export { getIModeloArchivoCompletoMock };
