import { FormGroup, AbstractControl } from '@angular/forms';

export enum TipoImagen {
  facturas = 'facturas',
  notasVentas = 'notasVenta'
}

export interface IModeloArchivo {
  nombreArchivoOriginal: string;
  nombreArchivo: string;
  ubicacion: string;
}

export interface IModeloArchivoCompleto extends IModeloArchivo {
  /**
   * url para peticion de imagen al servidor
   *
   * @type {string}
   * @memberof IModeloArchivo
   */
  url?: string;
}

/**
 * Marcar formGroup como touched
 *
 * @url https://stackoverflow.com/questions/40529817/reactive-forms-mark-fields-as-touched#44150793
 * @export
 * @param {FormGroup} formGroup
 */
export function markFormGroupTouched(formGroup: FormGroup) {
  (<any>Object).values(formGroup.controls).forEach((control: any) => {
    control.markAsTouched();
    if (control.controls) {
      this.markFormGroupTouched(control);
    }
  });
}
