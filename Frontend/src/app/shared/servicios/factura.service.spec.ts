/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FacturaService } from './factura.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('Service: Factura', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FacturaService]
    });
  });

  it('should create', inject([FacturaService], (service: FacturaService) => {
    expect(service).toBeTruthy();
  }));
});
