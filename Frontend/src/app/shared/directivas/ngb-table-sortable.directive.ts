import { Directive, Output, Input, EventEmitter, HostBinding, HostListener } from '@angular/core';
import { SortDirection, SortEvent } from '../ngb-table-sort.util';

@Directive({
  selector: 'th[appSortable]'
})
export class NgbTableSortableDirective {
  @Input()
  sortable: string;
  @Input()
  direction: SortDirection = '';
  @Output()
  sort = new EventEmitter<SortEvent>();

  @HostBinding('class.asc')
  get checkAscDireccition() {
    return this.direction === 'asc';
  }
  @HostBinding('class.desc')
  get checkDscDireccition() {
    return this.direction === 'desc';
  }

  rotateSort: { [key: string]: SortDirection } = { asc: 'desc', desc: '', '': 'asc' };

  constructor() {}

  @HostListener('click')
  rotate() {
    this.direction = this.rotateSort[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}
