import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudEtiquetaComponent } from './crud-etiqueta.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EtiquetaService } from '@app/shared/servicios';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { of } from 'rxjs';
import { getIEtiquetaMock, IEtiqueta } from '@app/core/models';
import { BusquedaCategoriaComponent } from '@app/shared/componentes/busqueda-categoria/busqueda-categoria.component';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

describe('CrudEtiquetaComponent', () => {
  let component: CrudEtiquetaComponent;
  let fixture: ComponentFixture<CrudEtiquetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientTestingModule, NgbTypeaheadModule],
      declarations: [CrudEtiquetaComponent, BusquedaCategoriaComponent],
      providers: [
        EtiquetaService,
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of(
              convertToParamMap({
                id: 'abcd1234'
              })
            )
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudEtiquetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia guardar etiqueta dado formulario válido', () => {
    // GIVEN
    const etiquetaService = fixture.debugElement.injector.get(EtiquetaService);
    const subscripcion = spyOn(etiquetaService, 'create').and.returnValue(
      of({ nombre: 'Comida', categoria: 'Poliperros', creado: 123, actualizado: 123, id: '123' })
    );
    component.etiquetaForm.get('nombre').setValue('Comida');
    component.etiquetaForm.get('categoria').setValue('Poliperros');

    // WHEN
    component.guardarEtiqueta();

    // ASSERT
    expect(subscripcion).toHaveBeenCalled();
    expect(component.etiquetaForm.value).toEqual({
      nombre: 'Comida',
      categoria: 'Poliperros',
      creado: 123,
      actualizado: 123,
      id: '123'
    });
  });

  it('deberia marcar formulario como touched dado formulario invalido al guardar etiqueta', () => {
    // GIVEN
    component.etiquetaForm.get('nombre').setValue('Comida');

    // WHEN
    component.guardarEtiqueta();

    // ASSERT
    expect(component.etiquetaForm.touched).toBeTruthy();
  });

  it('deberia actualizar etiqueta dado formulario válido', () => {
    // GIVEN
    const etiquetaService = fixture.debugElement.injector.get(EtiquetaService);
    const subscripcion = spyOn(etiquetaService, 'update').and.returnValue(
      of(getIEtiquetaMock({ nombre: 'Comida', categoria: 'Poliperros' }))
    );
    component.etiquetaForm.get('nombre').setValue('Comida');
    component.etiquetaForm.get('categoria').setValue('Poliperros');

    // WHEN
    component.actualizarEtiqueta();

    // ASSERT
    expect(subscripcion).toHaveBeenCalled();
    expect(component.etiquetaForm.value).toEqual({
      nombre: 'Comida',
      categoria: 'Poliperros',
      creado: '',
      actualizado: '',
      id: ''
    });
  });

  it('deberia marcar formulario como touched dado formulario invalido al actualizar etiqueta', () => {
    // GIVEN
    component.etiquetaForm.get('nombre').setValue('Comida');

    // WHEN
    component.actualizarEtiqueta();

    // ASSERT
    expect(component.etiquetaForm.touched).toBeTruthy();
  });

  it('deberia resetear el formulario al eliminar etiqueta', async(() => {
    // GIVEN
    const etiquetaService = fixture.debugElement.injector.get(EtiquetaService);
    const subscripcion = spyOn(etiquetaService, 'destroy').and.returnValue(
      of(getIEtiquetaMock({ nombre: 'Comida', categoria: 'Poliperros' }))
    );
    component.etiquetaForm.get('nombre').setValue('Comida');
    component.etiquetaForm.get('categoria').setValue('Poliperros');
    component.etiquetaForm.get('id').setValue('123456789');

    // WHEN
    component.eliminarEtiqueta();

    // ASSERT
    expect(subscripcion).toHaveBeenCalled();
    expect(component.etiquetaForm.get('nombre').value).toBe(null);
    expect(component.etiquetaForm.get('categoria').value).toBe(null);
  }));

  it('deberia descargar etiqueta dado un id', async(() => {
    // GIVEN
    const etiquetaService = fixture.debugElement.injector.get(EtiquetaService);
    const subscripcion = spyOn(etiquetaService, 'findOne').and.returnValue(
      of(getIEtiquetaMock({ nombre: 'Comida', categoria: 'Poliperros', id: '123' }))
    );
    const etiquetaForm = spyOn(component.etiquetaForm, 'setValue').and.callFake(() => {
      component.etiquetaForm.get('nombre').setValue('Comida');
      component.etiquetaForm.get('categoria').setValue('Poliperros');
      component.etiquetaForm.get('id').setValue('123');
    });

    // WHEN
    component.descargarEtiqueta('123');

    // ASSERT
    expect(subscripcion).toHaveBeenCalled();
    expect(etiquetaForm).toHaveBeenCalled();
    expect(component.etiquetaForm.get('nombre').value).toBe('Comida');
    expect(component.etiquetaForm.get('categoria').value).toBe('Poliperros');
    expect(component.etiquetaForm.get('id').value).toBe('123');
  }));

  it('deberia cargar etiqueta pasada por el @input() etiqueta', () => {
    // GIVEN
    const etiqueta: IEtiqueta = getIEtiquetaMock({ nombre: 'Comida', categoria: 'Poliperros' });
    etiqueta['creado'] = '';
    etiqueta['actualizado'] = '';

    // WHEN
    component.ngOnInit();
    component.cargarEtiqueta = etiqueta;

    // ASSERT
    expect(component.etiquetaForm.value).toEqual(etiqueta);
  });
});
