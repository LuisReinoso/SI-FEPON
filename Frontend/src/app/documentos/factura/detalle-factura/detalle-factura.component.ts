import { Component, OnInit } from '@angular/core';
import { IFactura } from '@app/core/models';

@Component({
  selector: 'app-detalle-factura',
  templateUrl: './detalle-factura.component.html',
  styleUrls: ['./detalle-factura.component.scss']
})
export class DetalleFacturaComponent implements OnInit {
  factura: IFactura = null;
  constructor() {}

  ngOnInit() {}

  cargarFacturaEnCrud(factura: IFactura) {
    this.factura = factura;
  }
}
