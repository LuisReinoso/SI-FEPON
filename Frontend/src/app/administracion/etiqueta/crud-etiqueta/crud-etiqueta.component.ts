import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Logger } from '@app/core';
import { EtiquetaService } from '@app/shared/servicios';
import { IEtiqueta } from '@app/core/models';
import { ActivatedRoute } from '@angular/router';
import { markFormGroupTouched } from '@app/shared/utilidades.util';
import { Subject } from 'rxjs';

const log = new Logger('Crud Etiqueta');

@Component({
  selector: 'app-crud-etiqueta',
  templateUrl: './crud-etiqueta.component.html',
  styleUrls: ['./crud-etiqueta.component.scss'],
  providers: [EtiquetaService]
})
export class CrudEtiquetaComponent implements OnInit {
  @Input('etiqueta')
  set cargarEtiqueta(etiqueta: IEtiqueta) {
    if (etiqueta) {
      this.etiqueta.next(etiqueta);
    }
  }

  etiquetaForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required]],
    categoria: ['', [Validators.required]],
    creado: [''],
    actualizado: [''],
    id: ['']
  });

  etiqueta = new Subject<IEtiqueta>();

  constructor(
    private fb: FormBuilder,
    private etiquetaService: EtiquetaService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['id']) {
        this.descargarEtiqueta(params['id']);
      }
    });
  }

  ngOnInit() {
    this.etiqueta.subscribe(etiqueta => {
      this.etiquetaForm.setValue(etiqueta);
    });
  }

  guardarEtiqueta() {
    if (this.etiquetaForm.valid) {
      const auxEtiqueta: IEtiqueta = {
        nombre: this.etiquetaForm.get('nombre').value,
        categoria: this.etiquetaForm.get('categoria').value
      };
      this.etiquetaService.create(auxEtiqueta).subscribe(
        (etiqueta: IEtiqueta) => {
          this.etiquetaForm.setValue(etiqueta);
        },
        (error: Object) => log.error(error)
      );
    } else {
      markFormGroupTouched(this.etiquetaForm);
    }
  }

  actualizarEtiqueta() {
    if (this.etiquetaForm.valid) {
      const auxEtiqueta: IEtiqueta = {
        nombre: this.etiquetaForm.get('nombre').value,
        categoria: this.etiquetaForm.get('categoria').value,
        id: this.etiquetaForm.get('id').value
      };
      this.etiquetaService
        .update(auxEtiqueta)
        .subscribe((etiqueta: IEtiqueta) => log.info(etiqueta), (error: Object) => log.error(error));
    } else {
      markFormGroupTouched(this.etiquetaForm);
    }
  }

  eliminarEtiqueta() {
    this.etiquetaService
      .destroy(this.etiquetaForm.value)
      .subscribe((etiqueta: IEtiqueta) => this.etiquetaForm.reset(), (error: Object) => log.info(error));
  }

  descargarEtiqueta(idEtiqueta: string) {
    this.etiquetaService
      .findOne(idEtiqueta)
      .subscribe((etiqueta: IEtiqueta) => this.etiquetaForm.setValue(etiqueta), (error: Object) => log.error(error));
  }
}
