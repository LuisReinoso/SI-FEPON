/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DetalleEtiquetaComponent } from './detalle-etiqueta.component';
import { CrudEtiquetaComponent } from '../crud-etiqueta/crud-etiqueta.component';
import { BusquedaEtiquetaComponent } from '@app/shared/componentes/busqueda-etiqueta/busqueda-etiqueta.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { of } from 'rxjs';
import { getIEtiquetaMock } from '@app/core/models';
import { BusquedaCategoriaComponent } from '@app/shared/componentes/busqueda-categoria/busqueda-categoria.component';

describe('DetalleEtiquetaComponent', () => {
  let component: DetalleEtiquetaComponent;
  let fixture: ComponentFixture<DetalleEtiquetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, NgbTypeaheadModule, HttpClientTestingModule],
      declarations: [
        DetalleEtiquetaComponent,
        CrudEtiquetaComponent,
        BusquedaEtiquetaComponent,
        BusquedaCategoriaComponent
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of(
              convertToParamMap({
                id: 'abcd1234'
              })
            )
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleEtiquetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia cargar etiqueta en crud', () => {
    // GIVEN
    const etiqueta = getIEtiquetaMock({ categoria: 'Poliperros', nombre: 'Comida' });

    // WHEN
    component.cargarEtiquetaEnCrud(etiqueta);

    // ASSETS
    expect(component.etiqueta).toEqual(etiqueta);
  });
});
