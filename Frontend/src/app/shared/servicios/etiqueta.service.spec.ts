/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EtiquetaService } from './etiqueta.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('Service: Etiqueta', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EtiquetaService]
    });
  });

  it('should create', inject([EtiquetaService], (service: EtiquetaService) => {
    expect(service).toBeTruthy();
  }));
});
