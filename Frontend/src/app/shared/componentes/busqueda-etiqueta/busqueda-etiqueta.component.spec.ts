/* tslint:disable:no-unused-variable */
import { async, inject, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BusquedaEtiquetaComponent } from './busqueda-etiqueta.component';
import { NgbTypeaheadModule, NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { EtiquetaService } from '@app/shared/servicios';
import { CrudEtiquetaService } from '@app/administracion/etiqueta/crud-etiqueta/crud-etiqueta.service';
import { CargarArchivosService } from '@app/shared/cargar-archivos/cargar-archivos.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { IEtiqueta, getIEtiquetaMock } from '@app/core/models';

describe('BusquedaEtiquetaComponent', () => {
  let component: BusquedaEtiquetaComponent;
  let fixture: ComponentFixture<BusquedaEtiquetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, NgbTypeaheadModule, HttpClientTestingModule, FormsModule, ReactiveFormsModule],
      declarations: [BusquedaEtiquetaComponent],
      providers: [EtiquetaService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusquedaEtiquetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia realizar una busqueda', inject([EtiquetaService], (etiquetaService: EtiquetaService) => {
    // GIVEN
    const search = of('poliperros');
    spyOn(etiquetaService, 'findWhere').and.returnValue(of([{ nombre: 'Comida', categoria: 'Poliperros' }]));

    // WHEN
    component.busquedaEtiqueta(search).subscribe(data => {
      // ASSERT
      expect(etiquetaService.findWhere).toHaveBeenCalled();
      expect(data).toEqual([{ nombre: 'Comida', categoria: 'Poliperros' }]);
    });
  }));

  it('deberia dar formato a la etiqueta', () => {
    // GIVEN
    const etiqueta: IEtiqueta = getIEtiquetaMock({ nombre: 'Alimentos' });

    // WHEN
    const nombreEtiqueta = component.formatter(etiqueta);

    // ASSERT
    expect(nombreEtiqueta).toBe(etiqueta.nombre);
  });

  it('deberia emitir la etiqueta seleccionada', () => {
    // GIVEN
    const etiqueta: IEtiqueta = getIEtiquetaMock({ nombre: 'Alimentos' });
    const typeaheadEvent: NgbTypeaheadSelectItemEvent = { item: etiqueta, preventDefault: null };
    spyOn(component.resultadoSeleccion, 'emit');

    // WHEN
    component.emitirSeleccionado(typeaheadEvent);

    // ASSERT
    expect(component.resultadoSeleccion.emit).toHaveBeenCalledWith(etiqueta);
  });
});
