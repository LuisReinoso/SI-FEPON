/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { NgbTableSortableDirective } from './ngb-table-sortable.directive';

describe('Directive: NgbTableSortable', () => {
  it('should create an instance', () => {
    const directive = new NgbTableSortableDirective();
    expect(directive).toBeTruthy();
  });
});
