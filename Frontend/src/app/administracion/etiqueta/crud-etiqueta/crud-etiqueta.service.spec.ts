/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CrudEtiquetaService } from './crud-etiqueta.service';

describe('Service: CrudEtiqueta', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CrudEtiquetaService]
    });
  });

  it('should ...', inject([CrudEtiquetaService], (service: CrudEtiquetaService) => {
    expect(service).toBeTruthy();
  }));
});
