/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { QueryList } from '@angular/core';

import { DashboardEtiquetaComponent } from './dashboard-etiqueta.component';
import { RouterModule, Router } from '@angular/router';
import { TagInputModule } from 'ngx-chips';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EtiquetaService } from '@app/shared/servicios';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { getIFacturaMock, getIEtiquetaMock } from '@app/core/models';
import { of } from 'rxjs';
import { NgbTableSortableDirective } from '@app/shared/directivas/ngb-table-sortable.directive';

describe('DashboardEtiquetaComponent', () => {
  let component: DashboardEtiquetaComponent;
  let fixture: ComponentFixture<DashboardEtiquetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        RouterModule,
        TagInputModule,
        NgbPaginationModule,
        NgxChartsModule
      ],
      declarations: [DashboardEtiquetaComponent],
      providers: [
        EtiquetaService,
        {
          provide: Router,
          useValue: {
            navigate: jasmine.createSpy('navigate')
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEtiquetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia navegar a detalle etiqueta', () => {
    const router = fixture.debugElement.injector.get(Router);
    component.navegarADetalleEtiqueta();
    expect(router.navigate).toHaveBeenCalledWith(['/etiqueta/detalle']);
  });

  it('deberia configurar las facturas dado un arreglo de facturas', () => {
    // GIVEN
    const facturasPaginador = [getIFacturaMock({})];

    // WHEN
    component.facturas = facturasPaginador;

    // THEN
    expect(component.facturas.length).toBe(1);
  });

  it('deberia descargar etiquetas dado un nombre', () => {
    // GIVEN
    const nombre = 'Alimentos';
    const etiquetaService = fixture.debugElement.injector.get(EtiquetaService);
    spyOn(etiquetaService, 'findWhere').and.returnValue(of([getIEtiquetaMock({ nombre: nombre })]));

    // WHEN
    const respuesta = component.descargarEtiquetas('Alimentos');

    // THEN
    respuesta.subscribe(etiqueta => expect(etiqueta).toEqual([getIEtiquetaMock({ nombre: nombre })]));
  });

  it('deberia ordenar las facturas dadas segun la columna: total y direccion: ascendente', () => {
    // GIVEN
    component.facturas = [getIFacturaMock({ total: 100 }), getIFacturaMock({ total: 50 })];
    component.headers = new QueryList<NgbTableSortableDirective>();
    component.headers.reset([
      {
        sortable: 'total',
        direction: '',
        sort: null,
        rotate: null,
        rotateSort: null,
        checkAscDireccition: null,
        checkDscDireccition: null
      }
    ]);

    // WHEN
    component.onSort({ column: 'total', direction: 'asc' });

    // THEN
    expect(component.facturas[0].total).toBe(50);
    expect(component.facturas[1].total).toBe(100);
  });

  it('deberia ordenar las facturas dadas segun la columna: comentario y direccion: ""', () => {
    // GIVEN
    component.facturas = [getIFacturaMock({ total: 100 }), getIFacturaMock({ total: 50 })];
    component.headers = new QueryList<NgbTableSortableDirective>();
    component.headers.reset([
      {
        sortable: 'comentario',
        direction: '',
        sort: null,
        rotate: null,
        rotateSort: null,
        checkAscDireccition: null,
        checkDscDireccition: null
      }
    ]);

    // WHEN
    component.onSort({ column: 'total', direction: '' });

    // THEN
    expect(component.facturas[0].total).toBe(100);
    expect(component.facturas[1].total).toBe(50);
  });

  it('deberia ordenar las facturas dadas segun la columna: total y direccion: descendente', () => {
    // GIVEN
    component.facturas = [getIFacturaMock({ total: 100 }), getIFacturaMock({ total: 50 })];
    component.headers = new QueryList<NgbTableSortableDirective>();
    component.headers.reset([
      {
        sortable: 'comentario',
        direction: '',
        sort: null,
        rotate: null,
        rotateSort: null,
        checkAscDireccition: null,
        checkDscDireccition: null
      }
    ]);

    // WHEN
    component.onSort({ column: 'total', direction: 'desc' });

    // THEN
    expect(component.facturas[0].total).toBe(100);
    expect(component.facturas[1].total).toBe(50);
  });
});
