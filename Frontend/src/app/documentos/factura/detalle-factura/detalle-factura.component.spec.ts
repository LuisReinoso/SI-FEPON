/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DetalleFacturaComponent } from './detalle-factura.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbTypeaheadModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CrudFacturaComponent } from '../crud-factura/crud-factura.component';
import { BusquedaFacturaComponent } from '@app/shared/componentes/busqueda-factura/busqueda-factura.component';
import { CargarArchivosComponent } from '@app/shared/cargar-archivos/cargar-archivos.component';
import { TagInputModule } from 'ngx-chips';
import { LoaderComponent } from '@app/shared';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { of } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { getIFacturaMock } from '@app/core/models';

describe('DetalleFacturaComponent', () => {
  let component: DetalleFacturaComponent;
  let fixture: ComponentFixture<DetalleFacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        NgbTypeaheadModule,
        HttpClientTestingModule,
        NgbDatepickerModule,
        TagInputModule,
        BrowserAnimationsModule
      ],
      declarations: [
        DetalleFacturaComponent,
        CrudFacturaComponent,
        BusquedaFacturaComponent,
        CargarArchivosComponent,
        LoaderComponent
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of(
              convertToParamMap({
                id: 'abcd1234'
              })
            )
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia cargar factura en crud', () => {
    // GIVEN
    const factura = getIFacturaMock({ id: 'abcd1234' });

    // WHEN
    component.cargarFacturaEnCrud(factura);

    // ASSETS
    expect(component.factura).toEqual(factura);
  });
});
