import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CoreModule, AuthenticationService, I18nService } from '@app/core';
import { LoginComponent } from './login.component';
import { of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule, RouterTestingModule, TranslateModule.forRoot(), ReactiveFormsModule, CoreModule],
      declarations: [LoginComponent],
      providers: [
        AuthenticationService,
        {
          provide: Router,
          useValue: {
            navigate: jasmine.createSpy('navigate')
          }
        },
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of({})
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia autenticarse', () => {
    // GIVEN
    const authenticationService = fixture.debugElement.injector.get(AuthenticationService);
    const router = fixture.debugElement.injector.get(Router);
    component.loginForm.controls.username.setValue('luis.reinoso@mail.com');
    component.loginForm.controls.password.setValue('123456789');
    spyOn(authenticationService, 'login').and.returnValue(of({ username: 'luis.reinoso@mail.com', token: '' }));

    // WHEN
    component.login();

    // THEN
    expect(router.navigate).toHaveBeenCalledWith(['/'], { replaceUrl: true });
  });

  it('deberia cambiar de idioma dado el codigo de lenguaje', () => {
    // GIVEN
    const i18nService = fixture.debugElement.injector.get(I18nService);
    const setLanguage = spyOnProperty(i18nService, 'language', 'set');
    const language = 'es-EC';

    // WHEN
    component.setLanguage(language);

    // THEN
    expect(setLanguage).toHaveBeenCalledWith(language);
  });
});
