import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { LoaderComponent } from './loader/loader.component';
import { CargarArchivosComponent } from './cargar-archivos/cargar-archivos.component';
import { BusquedaEtiquetaComponent } from './componentes/busqueda-etiqueta/busqueda-etiqueta.component';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { EtiquetaService } from './servicios';
import { BusquedaCategoriaComponent } from './componentes/busqueda-categoria/busqueda-categoria.component';
import { BusquedaFacturaComponent } from './componentes/busqueda-factura/busqueda-factura.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NgbTypeaheadModule],
  declarations: [
    LoaderComponent,
    CargarArchivosComponent,
    BusquedaEtiquetaComponent,
    BusquedaCategoriaComponent,
    BusquedaFacturaComponent
  ],
  exports: [
    LoaderComponent,
    CargarArchivosComponent,
    BusquedaEtiquetaComponent,
    BusquedaCategoriaComponent,
    BusquedaFacturaComponent,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [EtiquetaService]
})
export class SharedModule {}
