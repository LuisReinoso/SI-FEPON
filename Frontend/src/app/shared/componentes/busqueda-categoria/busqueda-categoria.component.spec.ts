/* tslint:disable:no-unused-variable */
import { async, inject, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusquedaCategoriaComponent } from './busqueda-categoria.component';
import { NgbTypeaheadModule, NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { EtiquetaService } from '@app/shared/servicios';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { IEtiqueta, getIEtiquetaMock } from '@app/core/models';

describe('BusquedaEtiquetaComponent', () => {
  let component: BusquedaCategoriaComponent;
  let fixture: ComponentFixture<BusquedaCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, NgbTypeaheadModule, HttpClientTestingModule, FormsModule, ReactiveFormsModule],
      declarations: [BusquedaCategoriaComponent],
      providers: [EtiquetaService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusquedaCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia realizar una busqueda', inject([EtiquetaService], (etiquetaService: EtiquetaService) => {
    // GIVEN
    const search = of('poliperros');
    spyOn(etiquetaService, 'findWhere').and.returnValue(of([{ nombre: 'Comida', categoria: 'Poliperros' }]));

    // WHEN
    component.busquedaCategoria(search).subscribe(data => {
      // ASSERT
      expect(etiquetaService.findWhere).toHaveBeenCalled();
      expect(data).toEqual([{ nombre: 'Comida', categoria: 'Poliperros' }]);
    });
  }));

  it('deberia dar formato a la etiqueta', () => {
    // GIVEN
    const etiqueta: IEtiqueta = getIEtiquetaMock({ nombre: 'Alimentos', categoria: 'Poliperros' });

    // WHEN
    const categoriaEtiqueta = component.formatter(etiqueta.categoria);

    // ASSERT
    expect(categoriaEtiqueta).toBe(etiqueta.categoria);
  });

  it('deberia emitir la categoria seleccionada', () => {
    // GIVEN
    const etiqueta: IEtiqueta = getIEtiquetaMock({ nombre: 'Alimentos', categoria: 'Poliperros' });
    const typeaheadEvent: NgbTypeaheadSelectItemEvent = { item: etiqueta, preventDefault: null };
    spyOn(component.resultadoSeleccion, 'emit');

    // WHEN
    component.emitirSeleccionado(typeaheadEvent);

    // ASSERT
    expect(component.resultadoSeleccion.emit).toHaveBeenCalledWith(etiqueta);
  });
});
