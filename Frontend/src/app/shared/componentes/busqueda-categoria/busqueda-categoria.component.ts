import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import Query from 'waterline-query-language-parser';
import { Observable, of } from 'rxjs';
import { EtiquetaService } from '@app/shared/servicios';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError, map } from 'rxjs/operators';
import { IEtiqueta } from '@app/core/models';
import { FormGroup } from '@angular/forms';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-busqueda-categoria',
  templateUrl: './busqueda-categoria.component.html',
  styleUrls: ['./busqueda-categoria.component.scss'],
  providers: [EtiquetaService]
})
export class BusquedaCategoriaComponent implements OnInit {
  @Input()
  etiqueta: IEtiqueta = null;
  @Input()
  group: FormGroup = null;
  @Input()
  label = 'Búsqueda categoría';
  @Input()
  icon = 'fa-search';
  @Input()
  placeholder = 'Ingrese categoria a buscar';
  @Output()
  resultadoSeleccion = new EventEmitter<IEtiqueta>();

  isBuscando = false;
  constructor(private etiquetaService: EtiquetaService) {}
  ngOnInit() {}
  formatter = (categoria: string) => categoria;

  busquedaCategoria = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => (this.isBuscando = true)),
      switchMap(nombre => {
        const query = new Query(`categoria: ${nombre}`);
        const options = {
          select: 'categoria'
        };
        return this.etiquetaService.findWhere(query.query, options).pipe(
          tap(() => (this.isBuscando = false)),
          map((etiquetas: IEtiqueta[]) => etiquetas.map(etiqueta => etiqueta.categoria)),
          catchError(() => {
            this.isBuscando = true;
            return of([]);
          })
        );
      }),
      tap(() => (this.isBuscando = false))
    );
  emitirSeleccionado(evento: NgbTypeaheadSelectItemEvent) {
    this.resultadoSeleccion.emit(evento.item);
  }
}
