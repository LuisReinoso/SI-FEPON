import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import Query from 'waterline-query-language-parser';
import { Observable, of } from 'rxjs';
import { FacturaService } from '@app/shared/servicios';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError } from 'rxjs/operators';
import { IFactura } from '@app/core/models';
import { FormGroup } from '@angular/forms';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-busqueda-factura',
  templateUrl: './busqueda-factura.component.html',
  styleUrls: ['./busqueda-factura.component.scss'],
  providers: [FacturaService]
})
export class BusquedaFacturaComponent implements OnInit {
  @Input()
  factura: IFactura = null;
  @Input()
  group: FormGroup = null;
  @Input()
  label = 'Búsqueda factura';
  @Input()
  icon = 'fa-search';
  @Input()
  placeholder = 'Ingrese factura a buscar';
  @Output()
  resultadoSeleccion = new EventEmitter<IFactura>();

  isBuscando = false;
  constructor(private facturaService: FacturaService) {}
  ngOnInit() {}
  formatter = (factura: IFactura) => factura.comentario;

  busquedaFactura = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => (this.isBuscando = true)),
      switchMap(nombre => {
        const query = new Query(
          `comentario: ${nombre} nombreEmisor: ${nombre} rucEmisor: ${nombre} numeroFactura: ${nombre}
          nombreReceptor: ${nombre} rucReceptor: ${nombre}`
        );
        return this.facturaService.findWhere(query.query).pipe(
          tap(() => (this.isBuscando = false)),
          catchError(() => {
            this.isBuscando = true;
            return of([]);
          })
        );
      }),
      tap(() => (this.isBuscando = false))
    );

  emitirSeleccionado(evento: NgbTypeaheadSelectItemEvent) {
    this.resultadoSeleccion.emit(evento.item);
  }
}
