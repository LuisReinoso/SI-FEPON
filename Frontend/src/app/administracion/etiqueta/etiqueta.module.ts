import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrudEtiquetaComponent } from './crud-etiqueta/crud-etiqueta.component';
import { EtiquetaRoutingModule } from '@app/administracion/etiqueta/etiqueta-routing.module';
import { SharedModule } from '@app/shared';
import { DashboardEtiquetaComponent } from './dashboard-etiqueta/dashboard-etiqueta.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TagInputModule } from 'ngx-chips';
import { NgbTableSortableDirective } from '@app/shared/directivas/ngb-table-sortable.directive';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { DetalleEtiquetaComponent } from './detalle-etiqueta/detalle-etiqueta.component';

@NgModule({
  imports: [CommonModule, SharedModule, EtiquetaRoutingModule, NgxChartsModule, TagInputModule, NgbPaginationModule],
  declarations: [CrudEtiquetaComponent, DashboardEtiquetaComponent, DetalleEtiquetaComponent, NgbTableSortableDirective]
})
export class EtiquetaModule {}
