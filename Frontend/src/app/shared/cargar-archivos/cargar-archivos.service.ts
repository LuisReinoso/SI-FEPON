import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse, HttpEvent } from '@angular/common/http';
import { Subject, Observable, Subscription } from 'rxjs';
import { IImagenFactura } from '@app/core/models';
import { TipoImagen } from '@app/shared/utilidades.util';
import { map } from 'rxjs/operators';

@Injectable()
export class CargarArchivosService {
  private _peticionesImagenes: Map<string, Subscription>;
  constructor(public http: HttpClient) {
    this.peticionesImagenes = new Map();
  }

  public upload(files: File[]): { [key: string]: Observable<IImagenFactura> } {
    const status = {};

    files.forEach(file => {
      const formData: FormData = new FormData();
      formData.append('imagen', file, file.name);

      const observableImg = this.http.post('api/v1/imagen/factura', formData).pipe(map(response => response['imagen']));

      this.peticionesImagenes.set(file.name, observableImg.subscribe());

      status[file.name] = observableImg;
    });
    return status;
  }

  public cancelarPeticiones(): void {
    for (const nombreArchivo of Array.from(this.peticionesImagenes.keys())) {
      const peticion = this.peticionesImagenes.get(nombreArchivo);
      peticion.unsubscribe();
    }
    this.peticionesImagenes.clear();
  }

  public reiniciarArregloPeticionesImagen(): void {
    this.peticionesImagenes.clear();
  }

  public cancelaPeticion(nombreArchivo: string) {
    const peticion = this.peticionesImagenes.get(nombreArchivo);
    peticion.unsubscribe();
  }

  public eliminarArchivo(tipo: TipoImagen, nombre: string): Observable<Object> {
    return this.http.delete('api/v1/imagen?nombre=' + nombre + '&&tipo=' + tipo);
  }

  /**
   * Getter peticionesImagenes
   * Contiene todas las peticiones de imagenes en estado subiendo
   * @return {Subscription[]}
   */
  public get peticionesImagenes(): Map<string, Subscription> {
    return this._peticionesImagenes;
  }

  /**
   * Setter peticionesImagenes
   * @param {Subscription[]} value
   */
  public set peticionesImagenes(value: Map<string, Subscription>) {
    this._peticionesImagenes = value;
  }
}
