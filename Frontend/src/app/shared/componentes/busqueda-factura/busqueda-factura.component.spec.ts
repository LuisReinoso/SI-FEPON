/* tslint:disable:no-unused-variable */
import { async, inject, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BusquedaFacturaComponent } from './busqueda-factura.component';
import { NgbTypeaheadModule, NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { FacturaService } from '@app/shared/servicios';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { getIFacturaMock, IFactura } from '@app/core/models';

describe('BusquedaEtiquetaComponent', () => {
  let component: BusquedaFacturaComponent;
  let fixture: ComponentFixture<BusquedaFacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, NgbTypeaheadModule, HttpClientTestingModule, FormsModule, ReactiveFormsModule],
      declarations: [BusquedaFacturaComponent],
      providers: [FacturaService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusquedaFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia realizar una busqueda', inject([FacturaService], (facturaService: FacturaService) => {
    // GIVEN
    const search = of('Gasto obligatorio por deuda anterior');
    spyOn(facturaService, 'findWhere').and.returnValue(
      of([getIFacturaMock({ comentario: 'Gasto obligatorio por deuda anterior' })])
    );

    // WHEN
    component.busquedaFactura(search).subscribe(data => {
      // ASSERT
      expect(facturaService.findWhere).toHaveBeenCalled();
      expect(data).toEqual([getIFacturaMock({ comentario: 'Gasto obligatorio por deuda anterior' })]);
    });
  }));

  it('deberia dar formato a la factura', () => {
    // GIVEN
    const factura: IFactura = getIFacturaMock({ comentario: 'Gasto obligatorio por deuda anterior' });

    // WHEN
    const comentarioFactura = component.formatter(factura);

    // ASSERT
    expect(comentarioFactura).toBe(factura.comentario);
  });

  it('deberia emitir la factura seleccionada', () => {
    // GIVEN
    const factura: IFactura = getIFacturaMock({ comentario: 'Gasto obligatorio por deuda anterior' });
    const typeaheadEvent: NgbTypeaheadSelectItemEvent = { item: factura, preventDefault: null };
    spyOn(component.resultadoSeleccion, 'emit');

    // WHEN
    component.emitirSeleccionado(typeaheadEvent);

    // ASSERT
    expect(component.resultadoSeleccion.emit).toHaveBeenCalledWith(factura);
  });
});
