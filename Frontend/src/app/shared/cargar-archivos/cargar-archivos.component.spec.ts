import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargarArchivosComponent } from '@app/shared/cargar-archivos/cargar-archivos.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Observable, Subject, of } from 'rxjs';
import { IImagenFactura } from '@app/core/models';
import { CargarArchivosService } from '@app/shared/cargar-archivos/cargar-archivos.service';
import { SharedModule } from '@app/shared/shared.module';
import { getIModeloArchivoCompletoMock } from '@app/core/models/modelo-archivo.mock';
import { environment } from '@env/environment';
import { IModeloArchivoCompleto } from '../utilidades.util';

describe('CargarArchivosComponent', () => {
  let servicio: CargarArchivosService;
  let component: CargarArchivosComponent;
  let fixture: ComponentFixture<CargarArchivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule]
    }).compileComponents();
    servicio = new CargarArchivosService(null);
    component = new CargarArchivosComponent(servicio);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargarArchivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // tslint:disable-next-line:max-line-length
  // https://stackoverflow.com/questions/47119426/how-to-set-file-objects-and-length-property-at-filelist-object-where-the-files-a#47172409
  it('deberia agregar imagenes seleccionadas a listas de archivos por subir', () => {
    // GIVEN
    const fileList = new ClipboardEvent('').clipboardData || new DataTransfer();
    fileList.items.add(new File(['jpg'], 'archivo-prueba.jpg'));
    component.file.nativeElement.files = fileList.files;

    // WHEN
    component.agregarImagenesSeleccionadasToArchivosPorSubir();

    // THEN
    expect(component.file.nativeElement.value).toBe('');
    expect(component.archivosPorSubir.length).toBe(1);
  });

  // xit('deberia cargar el archivo a través del archivos por subir', () => {
  //   const file = new File(['jpg'], 'archivo-prueba.jpg');
  //   const status: { [key: string]: Observable<IModeloArchivoCompleto> } = {
  //     'archivo-prueba.jpg': of(getIModeloArchivoCompletoMock())
  //   };
  //   spyOn(servicio, 'upload').and.returnValue(status);
  //   component.archivosPorSubir = [file];
  //   spyOn(of(getIModeloArchivoCompletoMock()), 'subscribe').and.returnValue(getIModeloArchivoCompletoMock());
  //   component.cargarTodasLasImagenes();
  //   expect(component.archivosPorSubir.length).toBe(0);
  // });

  it('deberia subir archivos seleccionados', () => {
    const file = new File(['nombre'], 'archivo-prueba.jpg', { lastModified: 1534721459534, type: 'image/jpeg' });

    const status: { [key: string]: Observable<IImagenFactura> } = {
      'archivo-prueba.jpg': new Subject<IImagenFactura>().asObservable()
    };
    spyOn(servicio, 'upload').and.returnValue(status);

    expect(servicio.upload([file])).toEqual(status);
  });

  it('deberia cargar archivos almacenados desde el input archivosAlmacenados', () => {
    // GIVEN
    const imagenes = [getIModeloArchivoCompletoMock(), getIModeloArchivoCompletoMock()];

    // WHEN
    component.cargarArchivosAlmacenados = imagenes;

    // THEN
    expect(component.archivosAlmacenados[0].url).toBe(`${imagenes[0].url}`);
  });
});
