import { IEtiqueta } from './etiqueta.model';

const getDefaults = (): IEtiqueta =>
  <IEtiqueta>{
    id: '',
    nombre: '',
    categoria: ''
  };

const getIEtiquetaMock = (p?: Partial<IEtiqueta>): IEtiqueta => {
  return <IEtiqueta>{
    ...getDefaults(),
    ...p
  };
};

export { getIEtiquetaMock };
