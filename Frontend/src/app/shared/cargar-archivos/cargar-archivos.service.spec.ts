import { TestBed, inject } from '@angular/core/testing';

import { CargarArchivosService } from '@app/shared/cargar-archivos/cargar-archivos.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of, Subject } from 'rxjs';
import { IImagenFactura } from '@app/core/models';
import { HttpClient } from '@angular/common/http';
import { getIModeloArchivoCompletoMock } from '@app/core/models/modelo-archivo.mock';
import { TipoImagen } from '../utilidades.util';

describe('CargarArchivosService', () => {
  let servicio: CargarArchivosService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CargarArchivosService]
    });
    const httpClient = new HttpClient(null);
    servicio = new CargarArchivosService(httpClient);
  });

  it('should be created', inject([CargarArchivosService], (service: CargarArchivosService) => {
    expect(service).toBeTruthy();
  }));

  xit('deberia retornar los objetos de progreso de subir archivo', inject([HttpClient], (http: HttpClient) => {
    // GIVEN
    const imagen = new Subject<IImagenFactura>();
    const mockImagenCompleto = getIModeloArchivoCompletoMock({ nombreArchivoOriginal: 'archivo-prueba.jpg' });
    const file = new File(['nombre'], 'archivo-prueba.jpg', { lastModified: 1534721459534, type: 'image/jpeg' });
    imagen.next(mockImagenCompleto);
    imagen.complete();

    spyOn(servicio.http, 'post').and.returnValue(of({ imagen: mockImagenCompleto }));

    // WHEN
    const progress = servicio.upload([file]);

    // THEN
    expect(progress).toEqual({
      'archivo-prueba.jpg': imagen.asObservable()
    });
  }));

  it('deberia limpiar todas las peticiones de imagenes', () => {
    servicio.reiniciarArregloPeticionesImagen();
    expect(servicio.peticionesImagenes.size).toBe(0);
  });

  it('deberia cancelar todas las peticiones de imagenes por subir', inject([HttpClient], (http: HttpClient) => {
    // GIVEN
    const imagen = new Subject<IImagenFactura>();
    const mockImagenCompleto = getIModeloArchivoCompletoMock({ nombreArchivoOriginal: 'archivo-prueba.jpg' });
    const file = new File(['nombre'], 'archivo-prueba.jpg', { lastModified: 1534721459534, type: 'image/jpeg' });
    spyOn(servicio.http, 'post').and.returnValue(of({ imagen: mockImagenCompleto }));
    imagen.next(mockImagenCompleto);
    imagen.complete();

    // WHEN
    servicio.upload([file]);
    servicio.cancelarPeticiones();

    // THEN
    expect(servicio.peticionesImagenes.size).toBe(0);
  }));

  it('deberia cancelar la peticicion de una imagen dado el nombre del archivo', () => {
    // GIVEN
    const mockImagenCompleto = getIModeloArchivoCompletoMock({ nombreArchivoOriginal: 'archivo-prueba.jpg' });
    const imagen = new Subject<IImagenFactura>();
    imagen.next(mockImagenCompleto);
    imagen.complete();
    servicio.peticionesImagenes.set('archivo-prueba.jpg', imagen.subscribe());

    // WHEN
    servicio.cancelaPeticion('archivo-prueba.jpg');

    // THEN
    expect(servicio.peticionesImagenes.get('archivo-prueba.jpg').closed).toBe(true);
  });

  it('deberia eliminar un archivo dado el tipo de imagen y el nombre', () => {
    // GIVEN
    const tipo = TipoImagen.facturas;
    const nombre = 'archivo-prueba.jpg';
    spyOn(servicio.http, 'delete');

    // WHEN
    servicio.eliminarArchivo(tipo, nombre);

    // THEN
    expect(servicio.http.delete).toHaveBeenCalledWith('api/v1/imagen?nombre=' + nombre + '&&tipo=' + tipo);
  });
});
