import { Injectable } from '@angular/core';
import { BaseRest } from './base-rest';
import { HttpClient } from '@angular/common/http';
import { Logger } from '@app/core';

const log = new Logger('Factura Service');

@Injectable()
export class FacturaService extends BaseRest {
  constructor(http: HttpClient) {
    super(http, 'Factura');
  }
}
