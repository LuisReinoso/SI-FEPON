import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import Query from 'waterline-query-language-parser';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { EtiquetaService } from '@app/shared/servicios';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError } from 'rxjs/operators';
import { IEtiqueta } from '@app/core/models';
import { FormGroup } from '@angular/forms';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-busqueda-etiqueta',
  templateUrl: './busqueda-etiqueta.component.html',
  styleUrls: ['./busqueda-etiqueta.component.scss'],
  providers: [EtiquetaService]
})
export class BusquedaEtiquetaComponent implements OnInit {
  @Input()
  etiqueta: IEtiqueta = null;
  @Input()
  group: FormGroup = null;
  @Output()
  resultadoSeleccion = new EventEmitter<IEtiqueta>();

  isBuscando = false;
  constructor(private etiquetaService: EtiquetaService) {}
  ngOnInit() {}
  formatter = (etiqueta: IEtiqueta) => etiqueta.nombre;

  busquedaEtiqueta = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => (this.isBuscando = true)),
      switchMap(nombre => {
        const query = new Query(`nombre: ${nombre} categoria: ${nombre}`);
        return this.etiquetaService.findWhere(query.query).pipe(
          tap(() => (this.isBuscando = false)),
          catchError(() => {
            this.isBuscando = true;
            return of([]);
          })
        );
      }),
      tap(() => (this.isBuscando = false))
    );

  emitirSeleccionado(evento: NgbTypeaheadSelectItemEvent) {
    this.resultadoSeleccion.emit(evento.item);
  }
}
