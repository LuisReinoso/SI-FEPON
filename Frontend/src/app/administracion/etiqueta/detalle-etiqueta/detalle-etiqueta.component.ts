import { Component, OnInit } from '@angular/core';
import { IEtiqueta } from '@app/core/models';

@Component({
  selector: 'app-detalle-etiqueta',
  templateUrl: './detalle-etiqueta.component.html',
  styleUrls: ['./detalle-etiqueta.component.scss']
})
export class DetalleEtiquetaComponent implements OnInit {
  etiqueta: IEtiqueta = null;
  constructor() {}

  ngOnInit() {}

  cargarEtiquetaEnCrud(etiqueta: IEtiqueta) {
    this.etiqueta = etiqueta;
  }
}
