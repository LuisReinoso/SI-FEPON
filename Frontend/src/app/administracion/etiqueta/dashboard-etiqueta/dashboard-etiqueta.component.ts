import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import * as chartsData from './../../../shared/ngx-charts.util';
import { IEtiqueta, IFactura, getIFacturaMock } from '@app/core/models';
import { Observable } from 'rxjs';
import { EtiquetaService } from '@app/shared/servicios';
import Query from 'waterline-query-language-parser';
import { SortEvent, compare } from '@app/shared/ngb-table-sort.util';
import { NgbTableSortableDirective } from '@app/shared/directivas/ngb-table-sortable.directive';
import { ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-etiqueta',
  templateUrl: './dashboard-etiqueta.component.html',
  styleUrls: ['./dashboard-etiqueta.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardEtiquetaComponent implements OnInit {
  resultados: any = [
    {
      name: 'Poliperros',
      series: [
        {
          name: 'Ene',
          value: 730000
        },
        {
          name: 'Feb',
          value: 840000
        },
        {
          name: 'Mar',
          value: 70000
        },
        {
          name: 'Jun',
          value: 850000
        },
        {
          name: 'Jul',
          value: 844400
        },
        {
          name: 'Ago',
          value: 2946000
        },
        {
          name: 'Sep',
          value: 2640000
        },
        {
          name: 'Oct',
          value: 1574000
        },
        {
          name: 'Nov',
          value: 3648000
        },
        {
          name: 'Dic',
          value: 1584000
        }
      ]
    },
    {
      name: 'Limpieza',
      series: [
        {
          name: 'Ene',
          value: 154852
        },
        {
          name: 'Feb',
          value: 354840
        },
        {
          name: 'Mar',
          value: 444840
        },
        {
          name: 'Jun',
          value: 685420
        },
        {
          name: 'Jul',
          value: 781200
        },
        {
          name: 'Ago',
          value: 9821540
        },
        {
          name: 'Sep',
          value: 1548812
        },
        {
          name: 'Oct',
          value: 1548510
        },
        {
          name: 'Nov',
          value: 1578120
        },
        {
          name: 'Dic',
          value: 1668741
        }
      ]
    }
  ];

  // options
  lineChartShowXAxis = chartsData.lineChartShowXAxis;
  lineChartShowYAxis = chartsData.lineChartShowYAxis;
  lineChartGradient = chartsData.lineChartGradient;
  lineChartShowLegend = chartsData.lineChartShowLegend;
  lineChartShowXAxisLabel = chartsData.lineChartShowXAxisLabel;
  lineChartXAxisLabel = chartsData.lineChartXAxisLabel;
  lineChartShowYAxisLabel = chartsData.lineChartShowYAxisLabel;
  lineChartYAxisLabel = chartsData.lineChartYAxisLabel;

  lineChartColorScheme = chartsData.lineChartColorScheme;

  // line, area
  lineChartAutoScale = chartsData.lineChartAutoScale;
  lineChartLineInterpolation = chartsData.lineChartLineInterpolation;

  etiquetasAComparar: IEtiqueta[] = [];
  _facturas: IFacturaPaginator[] = [
    getIFacturaMock({ comentario: 'Alimentos para perros', total: 500, fecha: new Date().getTime() }),
    getIFacturaMock({ comentario: 'Alimentos para gatos', total: 100, fecha: new Date().getTime() }),
    getIFacturaMock({ comentario: 'Alimentos para perros', total: 300, fecha: new Date().getTime() }),
    getIFacturaMock({ comentario: 'Alimentos para gatos', total: 120, fecha: new Date().getTime() }),
    getIFacturaMock({ comentario: 'Alimentos para perros', total: 450, fecha: new Date().getTime() }),
    getIFacturaMock({ comentario: 'Alimentos para gatos', total: 100, fecha: new Date().getTime() }),
    getIFacturaMock({ comentario: 'Alimentos para perros', total: 520, fecha: new Date().getTime() }),
    getIFacturaMock({ comentario: 'Alimentos para gatos', total: 200, fecha: new Date().getTime() }),
    getIFacturaMock({ comentario: 'Alimentos para perros', total: 530, fecha: new Date().getTime() }),
    getIFacturaMock({ comentario: 'Alimentos para gatos', total: 405, fecha: new Date().getTime() })
  ];

  @ViewChildren(NgbTableSortableDirective)
  headers: QueryList<NgbTableSortableDirective>;

  page = 1;
  pageSize = 4;
  collectionSize = this._facturas.length;

  get facturas(): IFacturaPaginator[] {
    return this._facturas
      .map((factura, i) => ({ indice: i + 1, ...factura }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  set facturas(facturas: IFacturaPaginator[]) {
    this._facturas = facturas;
  }

  constructor(private etiquetaService: EtiquetaService, private router: Router) {}

  ngOnInit() {}

  public descargarEtiquetas = (nombre: string): Observable<IEtiqueta[]> => {
    const query = new Query(`nombre: ${nombre} categoria: ${nombre}`);
    return <Observable<IEtiqueta[]>>this.etiquetaService.findWhere(query.query);
  };

  onSort({ column, direction }: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    if (direction === '') {
      this.facturas = this._facturas;
    } else {
      this.facturas = [...this._facturas].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  navegarADetalleEtiqueta() {
    this.router.navigate(['/etiqueta/detalle']);
  }
}

export interface IFacturaPaginator extends IFactura {
  indice?: number;
}
